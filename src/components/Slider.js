import React from "react";
import {
  MainSlider,
  SliderWrapper,
  SliderBubble,
  Text,
} from "./styled-components/Slider";

export default function Slider() {
  return (
    <MainSlider>
      <SliderWrapper>
        <SliderBubble />
        <Text>Obese</Text>
      </SliderWrapper>
    </MainSlider>
  );
}
