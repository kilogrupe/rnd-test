import React from "react";
import {
  CalendarContainer,
  Weekday,
  Number,
  CalendarWrapper,
  Title,
  Subtitle,
  Weight,
} from "./styled-components/Calendar";
const data = [
  { day: "mon", number: 1 },
  { day: "tue", number: 2 },
  { day: "wed", number: 3 },
  { day: "thu", number: 4 },
  { day: "fri", number: 5 },
  { day: "sat", number: 6 },
  { day: "sun", number: 7 },
];

export default function Calendar() {
  return (
    <>
      <CalendarContainer>
        {data.map((day) => (
          <CalendarWrapper key={`${day.day}_${day.number}`}>
            <Weekday>{day.day}</Weekday>
            <Number>{day.number}</Number>
          </CalendarWrapper>
        ))}
      </CalendarContainer>
      <Title>
        -5<Weight>kg</Weight>
      </Title>
      <Subtitle>After first week</Subtitle>
    </>
  );
}
