import React from "react";
import graph from "assets/images/graph.svg";
import {
  GraphContainer,
  DataContainer,
  DataDate,
  DataTitle,
  GraphTitleContainer,
  GraphNumber,
} from "components/styled-components/Graph";

const data = [51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 61];

export default function Graph() {
  return (
    <div>
      <GraphTitleContainer>
        {data.map((number, index) => (
          <GraphNumber key={`${index}_${number}`}>{number}</GraphNumber>
        ))}
      </GraphTitleContainer>
      <GraphContainer>
        <img src={graph} alt="graph" />
        <DataContainer Top="-48" Left="28">
          <DataDate>NOV 2019</DataDate>
          <DataTitle>64 kg</DataTitle>
        </DataContainer>
        <DataContainer Top="57" Right="50">
          <DataDate>JAN 2020</DataDate>
          <DataTitle>56 kg</DataTitle>
        </DataContainer>
      </GraphContainer>
    </div>
  );
}
