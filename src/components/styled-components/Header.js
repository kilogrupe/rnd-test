import styled from "styled-components";

export const Header = styled.header`
  display: flex;
  justify-content: center;
  background-color: #027bff;
`;

export const Toolbar = styled.nav`
  padding-left: 24px;
  padding-right: 24px;
  min-height: 80px;
  max-width: 1100px;
  width: 100%;
  display: flex;
  align-items: center;
`;

export const Brand = styled.div`
  height: 34px;
  flex-grow: 1;
`;
