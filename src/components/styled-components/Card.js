import styled from "styled-components";

export const Card = styled.div`
  padding: ${(props) => props.padding}px;
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  margin-bottom: ${(props) => props.marginBottom}px;
  overflow: hidden;
  background: #ffffff;
  box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.05);
  border-radius: 10px;
`;
