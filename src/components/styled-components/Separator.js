import styled from "styled-components";

export const VerticalSeparator = styled.div`
  display: inline-block;
  width: 1px;
  height: 32px;
  background-color: #222222;
  opacity: 0.2;
  margin: 0px 24px 0px 29px;
`;

export const HorizontalSeparator = styled.div`
  display: block;
  width: 100%;
  height: 1px;
  background: #e3e3e3;
  opacity: 0.8;
  margin: 24px 0px;
`;

export const WomenSeparator = styled.div`
  display: inline-block;
  background-color: rgba(0, 0, 0, 0.1);
  height: 1.3px;
  transform: rotate(-180deg);
  width: 58.98px;
  margin-right: 15px;
`;
