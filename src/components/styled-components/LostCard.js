import styled from "styled-components";

export const Title = styled.p`
  margin: 0px;
  display: inline-block;
  font-family: Cabin;
  font-style: normal;
  font-weight: bold;
  font-size: 3.375rem;
  line-height: 66px;
  color: #31c062;
`;

export const Percent = styled.span`
  font-family: Cabin;
  font-style: normal;
  font-weight: bold;
  font-size: 1.625rem;
  line-height: 32px;
  color: #31c062;
`;

export const Subtitle = styled.p`
  font-family: Cabin;
  font-style: normal;
  font-weight: normal;
  font-size: 1rem;
  line-height: 24px;
  text-align: center;
  color: #4e4e4e;
  mix-blend-mode: normal;
`;

export const AccentColor = styled.span`
  color: #31c062;
`;
