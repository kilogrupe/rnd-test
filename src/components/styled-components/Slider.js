import styled from "styled-components";

export const MainSlider = styled.div`
  position: relative;
  margin-top: 28px;
  margin-bottom: 25px;
  width: 100%;
  height: 8px;
  background: #e2e3e9;
  border-radius: 100px;
`;

export const SliderWrapper = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  align-items: center;
  right: 60px;
  top: -12px;
`;

export const SliderBubble = styled.div`
  width: 32px;
  height: 32px;
  background: #027bff;
  border: 4px solid #ffffff;
  box-sizing: border-box;
  border-radius: 50%;
`;

export const Text = styled.p`
  margin: 0px;
  font-family: Lato;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 24px;
  color: #222222;
`;
