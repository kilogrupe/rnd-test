import styled from "styled-components";

export const GraphTitleContainer = styled.div`
  margin-top: 15px;
  margin-bottom: 70px;
  display: flex;
  flex-direction: row;
`;

export const GraphNumber = styled.span`
  font-family: Cabin;
  font-style: normal;
  font-weight: 500;
  font-size: 1.2rem;
  line-height: 24px;
  color: #222222;
  opacity: 0.4;
  padding: 0px 22px;
`;

export const GraphContainer = styled.div`
  position: relative;
  margin-bottom: -5px;
`;

export const DataContainer = styled.div`
  top: ${(props) => props.Top}px;
  left: ${(props) => props.Left}px;
  right: ${(props) => props.Right}px;
  position: absolute;
  text-align: center;
  &:after {
    content: " ";
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    width: 16px;
    height: 16px;
    background: #31c062;
    border-radius: 50%;
  }
`;

export const DataDate = styled.p`
  margin: 0px;
  font-family: Cabin;
  font-style: normal;
  font-weight: bold;
  font-size: 0.8rem;
  line-height: 17px;
  letter-spacing: 3.33333px;
  text-transform: uppercase;
  color: #222222;
  opacity: 0.4;
`;

export const DataTitle = styled.p`
  margin: 0px;
  font-family: Cabin;
  font-style: normal;
  font-weight: 500;
  font-size: 1.25rem;
  line-height: 24px;
  color: #222222;
`;
