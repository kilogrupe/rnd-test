import styled from "styled-components";

export const H2 = styled.h2`
  text-align: ${(props) => props.align};
  margin-bottom: 15px;
  font-family: "Lato";
  font-style: normal;
  font-weight: bold;
  font-size: 2.5rem;
  line-height: 48px;
`;

export const H3 = styled.h3`
  font-family: "Lato";
  font-weight: bold;
  font-size: 2rem;
  color: #222222;
`;

export const Title1 = styled.p`
  margin-bottom: 0px;
  font-family: "Lato";
  font-style: normal;
  font-weight: bold;
  font-size: 1.9rem;
  line-height: 24px;
  color: #222222;
  opacity: 1;
`;

export const Title2 = styled.p`
  font-family: "Lato";
  font-style: normal;
  font-weight: bold;
  font-size: 1.5rem;
  line-height: 24px;
  color: #222222;
  opacity: 0.6;
`;

export const Body1 = styled.p`
  margin: 0px;
  opacity: ${(props) => props.opacity};
  margin-bottom: ${(props) => props.marginBottom}px;
  font-family: "Lato";
  font-style: normal;
  font-weight: normal;
  font-size: 1.1rem;
  line-height: 26px;
  color: #1a1e27;
`;

export const Body2 = styled.p`
  text-align: ${(props) => props.align};
  margin: 0;
  font-family: "Lato";
  font-style: normal;
  font-weight: 500;
  font-size: 1rem;
  line-height: 19px;
  color: #222222;
  padding: 7px;
  align-self: flex-end;
`;

export const Number1 = styled.p`
  margin: 0;
  font-family: Lato;
  font-style: normal;
  font-weight: bold;
  font-size: 2rem;
  line-height: 38px;
  color: #222222;
`;

export const Number2 = styled.p`
  margin: 0;
  font-family: Lato;
  font-style: normal;
  font-weight: bold;
  font-size: 2.5rem;
  line-height: 48px;
  color: #222222;
`;

export const AccentColor = styled.span`
  color: #31c062;
`;

export const Signature = styled.div`
  position: absolute;
  bottom: -25px;
  left: 160px;
`;
