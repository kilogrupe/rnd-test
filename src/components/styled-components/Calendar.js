import styled from "styled-components";

export const CalendarContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const CalendarWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 0px 8px;
  &:after {
    position: absolute;
    bottom: -10px;
    left: 50%;
    transform: translateX(-50%);
    content: " ";
    width: 6px;
    height: 6px;
    background: #a7a7a7;
    border-radius: 50%;
  }
`;

export const Weekday = styled.p`
  margin-bottom: 6px;
  font-family: Cabin;
  font-style: normal;
  font-weight: bold;
  font-size: 0.75rem;
  line-height: 15px;
  text-align: center;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #222222;
  opacity: 0.4;
`;

export const Number = styled.p`
  margin: 0px;
  font-family: Cabin;
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #222222;
`;

export const Title = styled.p`
  margin-bottom: 12px;
  font-family: Cabin;
  font-style: normal;
  font-weight: bold;
  font-size: 3.375rem;
  text-align: center;
  line-height: 66px;
  color: #31c062;
`;

export const Weight = styled.span`
  font-family: Cabin;
  font-style: normal;
  font-weight: bold;
  font-size: 1.625rem;
  line-height: 32px;
  color: #31c062;
  padding-left: 6px;
`;

export const Subtitle = styled.p`
  margin: 0px;
  font-family: Cabin;
  font-style: normal;
  font-weight: normal;
  font-size: 1rem;
  line-height: 24px;
  text-align: center;
  color: #222222;
  mix-blend-mode: normal;
  opacity: 0.8;
`;
