import styled from "styled-components";

export const Background = styled.div`
  background-color: ${(props) => props.backgroundColor};
`;

export const Container = styled.div`
  min-height: ${(props) => props.minHeight}px;
  max-width: 1100px;
  margin: auto;
  padding: 0px 24px;
  width: 100%;
`;

export const Box = styled.div`
  position: ${(props) => props.position};
  display: ${(props) => props.display};
  flex-direction: ${(props) => props.flexDirection};
  justify-content: ${(props) => props.justify};
  align-items: ${(props) => props.align};
  padding: ${(props) => props.spacing}px 0px;
  width: ${(props) => props.width}px;
  max-width: ${(props) => props.maxWidth}px;
  height: ${(props) => props.height};
  margin-top: ${(props) => props.marginTop}px;
  margin-left: ${(props) => props.marginLeft}px;
  margin-bottom: ${(props) => props.marginBottom}px;
  text-align: ${(props) => props.textAlign};
`;

export const Grid2 = styled.div`
  margin-top: ${(props) => props.marginTop}px;
  grid-gap: ${(props) => props.gap}px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

export const GridHero = styled.div`
  grid-gap: ${(props) => props.gap}px;
  padding: ${(props) => props.spacing}px 0px;
  display: grid;
  grid-template-columns: 1fr 2fr;
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

export const Column = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;
