import styled from "styled-components";

export const Button = styled.button`
  color: ${(props) => (props.primary ? "#FFFFFF" : "#027BFF")};
  background: ${(props) => (props.primary ? "#027BFF" : "#FFFFFF")};

  font-family: "Lato";
  font-style: normal;
  font-weight: bold;
  font-size: 1.1rem;
  text-transform: uppercase;
  border: unset;
  border-radius: 10px;
  padding: 11px 45px;
`;
