import React from "react";
import {
  Title,
  Percent,
  Subtitle,
  AccentColor,
} from "components/styled-components/LostCard";
import { Box } from "./styled-components/Grid";

export default function LostCard() {
  return (
    <Box
      display="flex"
      flexDirection="column"
      align="center"
      justify="center"
      height="100%"
    >
      <div>
        <Title>87</Title>
        <Percent>%</Percent>
      </div>
      <Subtitle>
        Similar people <AccentColor>lost</AccentColor> more than{" "}
        <AccentColor>10kg</AccentColor> on Fitter
      </Subtitle>
    </Box>
  );
}
