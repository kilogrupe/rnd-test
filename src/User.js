export const User = {
  years: 25,
  height: 169,
  weight: 64,
  bmi: 55.56,
  water: 1.7,
  metabolic: 29,
  kcal: "2300 - 2400",
  upperBody: 11,
  waist: 8,
  thighs: 20,
  legs: 16,
};
