import React from "react";
import { Header, Toolbar, Brand } from "components/styled-components/Header";
import { Button } from "components/styled-components/Button";
import logo from "assets/images/logo.png";
import InfoHeaderView from "./InfoHeaderView";
import HeroView from "./HeroView";
import PersonalSummaryView from "./PersonalSummaryView";
import BodyView from "./BodyView";
import PromiseView from "./PromiseView";

export default function HeroSummery() {
  return (
    <>
      <Header>
        <Toolbar>
          <Brand>
            <img src={logo} alt="Logo" />
          </Brand>
          <Button>start now</Button>
        </Toolbar>
      </Header>
      <InfoHeaderView />
      <HeroView />
      <PersonalSummaryView />
      <BodyView />
      <PromiseView />
    </>
  );
}
