import React from "react";
import {
  Background,
  Box,
  Container,
  Grid2,
} from "components/styled-components/Grid";
import {
  H2,
  Body2,
  Title1,
  Title2,
  Body1,
  AccentColor,
} from "components/styled-components/Typography";
import { WomenSeparator } from "components/styled-components/Separator";
import women from "assets/images/women.svg";

export default function BodyView() {
  return (
    <Background backgroundColor={"#F7F9FF"}>
      <Container>
        <Box spacing="75">
          <H2 align="center">Here’s what you can expect</H2>
          <Body2 align="center">Your body change estimations</Body2>
          <Grid2>
            <Box display="flex" flexDirection="row">
              <img src={women} alt="women" />
              <Box display="flex" flexDirection="column" marginLeft="-31">
                <Box display="flex" align="center" marginTop="56">
                  <WomenSeparator />
                  <Body1>Upper body -11%</Body1>
                </Box>
                <Box display="flex" align="center" marginTop="51">
                  <WomenSeparator />
                  <Body1>Waist -8%</Body1>
                </Box>
                <Box display="flex" align="center" marginTop="77">
                  <WomenSeparator />
                  <Body1>Thighs -20%</Body1>
                </Box>
                <Box display="flex" align="center" marginTop="61">
                  <WomenSeparator />
                  <Body1>Legs -16%</Body1>
                </Box>
              </Box>
            </Box>
            <Box
              display="flex"
              flexDirection="column"
              justify="center"
              align="flex-start"
            >
              <Title1>
                <AccentColor>Get ready to change your jeans size!</AccentColor>
              </Title1>
              <Box marginTop="13" maxWidth="400">
                <Body1>
                  Top performing users changed 2 sizes in 1 month. Prepare your
                  skinny jeans back!
                </Body1>
              </Box>
            </Box>
          </Grid2>
        </Box>
      </Container>
    </Background>
  );
}
