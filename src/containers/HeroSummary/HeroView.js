import React from "react";
import {
  Background,
  Container,
  GridHero,
  Box,
  Grid2,
} from "components/styled-components/Grid";
import {
  H2,
  Body1,
  AccentColor,
} from "components/styled-components/Typography";
import { Card } from "components/styled-components/Card";
import { Button } from "components/styled-components/Button";
import Graph from "components/Graph";
import LostCard from "components/LostCard";
import Calendar from "components/Calendar";
export default function HeroView() {
  return (
    <Background backgroundColor={"#F7F9FF"}>
      <Container>
        <GridHero gap="55" spacing="83">
          <Box display="flex" flexDirection="column">
            <H2>
              Based on your answers, you can
              <AccentColor> lose 8lbs </AccentColor>in 1 month
            </H2>
            <Body1 marginBottom="20">
              We’ve evaluated your answers and prepared a summary out of 10.243
              people similar to you
            </Body1>
            <Box>
              <Button primary>start now</Button>
            </Box>
          </Box>
          <Box>
            <Card width="100%" marginBottom="33">
              <Graph />
            </Card>
            <Grid2 gap="22">
              <Card padding="50">
                <LostCard />
              </Card>
              <Card padding="32">
                <Calendar />
              </Card>
            </Grid2>
          </Box>
        </GridHero>
      </Container>
    </Background>
  );
}
