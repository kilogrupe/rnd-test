import React from "react";
import { Container, Box, Grid2 } from "components/styled-components/Grid";
import {
  H2,
  Body1,
  Title2,
  Number1,
} from "components/styled-components/Typography";
import { Card } from "components/styled-components/Card";
import { HorizontalSeparator } from "components/styled-components/Separator";
import waterImg from "assets/images/water.svg";
import metabolicImg from "assets/images/metabolic.svg";
import calorieImg from "assets/images/calorie.svg";
import { User } from "User";
import Slider from "components/Slider";

export default function PersonalSummaryView() {
  const { bmi, water, metabolic, kcal } = User;
  return (
    <Container>
      <Box spacing={75}>
        <H2 align="center">Personal summary</H2>
        <Grid2 gap="48" marginTop="60">
          <Card padding="32">
            <Title2>Your BMI</Title2>
            <HorizontalSeparator />
            <Number1>{bmi}</Number1>
            <Slider />
          </Card>
          <Card padding="32">
            <Title2>Your water balance</Title2>
            <HorizontalSeparator />
            <Box display="flex">
              <Box width="64">
                <img src={waterImg} alt="water" />
              </Box>
              <Box>
                <Body1 opacity="0.6">Your daily minimum is</Body1>
                <Number1>{water} L</Number1>
              </Box>
            </Box>
          </Card>
          <Card padding="32">
            <Title2>Your metabolic age</Title2>
            <HorizontalSeparator />
            <Box display="flex">
              <Box width="68">
                <img src={metabolicImg} alt="metabolic" />
              </Box>
              <Box>
                <Body1 opacity="0.6">
                  Slightly higher than your actual age
                </Body1>
                <Number1>{metabolic} years</Number1>
              </Box>
            </Box>
          </Card>
          <Card padding="32">
            <Title2>Your daily calorie intake</Title2>
            <HorizontalSeparator />
            <Box display="flex">
              <Box width="64">
                <img src={calorieImg} alt="calorie" />
              </Box>
              <Box>
                <Body1 opacity="0.6">Recommended to achieve your goals</Body1>
                <Number1>{kcal} kcal</Number1>
              </Box>
            </Box>
          </Card>
        </Grid2>
      </Box>
    </Container>
  );
}
