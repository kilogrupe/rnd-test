import React from "react";
import { H2, Body2, Signature } from "components/styled-components/Typography";
import { Box, Container } from "components/styled-components/Grid";
import { Button } from "components/styled-components/Button";
import signature from "assets/images/signature.svg";

export default function PromiseView() {
  return (
    <Container>
      <Box spacing="76" display="flex" justify="center">
        <Box maxWidth="700" textAlign="center">
          <H2 align="center">Our promise</H2>
          <Box position="relative" marginBottom="59">
            <Body2 align="justify">
              We believe in a balanced way to lose weight and keep the weight
              off. We know that most of the diets or weight loss programs that
              you have tried in the past don’t work - they are just too hard to
              follow. Enormous food cravings, friends that visit you with a
              bottle of wine, that freshly baked cheesecake... Everything
              affects your journey and if your diet is not easy-to-follow, you
              are not going to achieve your results. <br />
              <br /> Don’t blame yourself. It’s really hard. That’s why our
              nutritionists and personal coaches work around the clock to
              prepare the most effective plans you love. We want to make sure
              that it becomes a part of your life. We don’t change habits, we
              help you to improve them. <br />
              <br /> Try it out and we guarantee you will be satisfied. <br />
              <br />
              Head of Nutrition, <br />
              Christine Ellis
            </Body2>
            <Signature>
              <img src={signature} alt="signature" />
            </Signature>
          </Box>
          <Button primary>start now</Button>
        </Box>
      </Box>
    </Container>
  );
}
