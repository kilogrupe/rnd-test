import React from "react";
import {
  Container,
  Grid2,
  Column,
  Box,
} from "components/styled-components/Grid";
import { H3, Number2, Body2 } from "components/styled-components/Typography";
import { VerticalSeparator } from "components/styled-components/Separator";
import { User } from "User";

export default function InfoHeaderView() {
  const { years, height, weight } = User;
  return (
    <Container minHeight={90}>
      <Grid2>
        <H3>Your profile summary</H3>
        <Column>
          <Box display="flex" align="center">
            <Number2>{years}</Number2>
            <Body2>years old</Body2>
            <VerticalSeparator />
            <Number2>{height}</Number2>
            <Body2>cm</Body2>
            <VerticalSeparator />
            <Number2>{weight}</Number2>
            <Body2>kg</Body2>
          </Box>
        </Column>
      </Grid2>
    </Container>
  );
}
